/*
 * GetEventsUseCase.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:47
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.domain.useCase.getEvents

import com.us.phunapp.domain.Either
import com.us.phunapp.domain.UseCase
import com.us.phunapp.events.domain.EventRepository

class GetEventsUseCase(
    private val repository: EventRepository
): UseCase<GetEventsResponse, GetEventsParams, GetEventsFailure>() {

    /** */
    override suspend fun run(
        params: GetEventsParams
    ): Either<GetEventsFailure, GetEventsResponse> =
        repository.getEvents()

}
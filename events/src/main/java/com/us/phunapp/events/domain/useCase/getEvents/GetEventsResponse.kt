/*
 * GetEventsResponse.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:46
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.domain.useCase.getEvents

import com.us.phunapp.events.domain.entity.EventItem

data class GetEventsResponse(
    val events: List<EventItem>
)
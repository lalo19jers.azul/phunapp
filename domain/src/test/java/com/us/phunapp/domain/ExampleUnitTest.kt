/*
 * ExampleUnitTest.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:05
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.domain

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}
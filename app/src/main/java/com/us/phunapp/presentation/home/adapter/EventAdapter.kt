/*
 * EventAdapter.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 01:18
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation.home.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.us.phunapp.events.domain.entity.EventItem

class EventAdapter(
    private val onRowClick: (EventItem) -> Unit
) : ListAdapter<EventItem, EventViewHolder>(EventDiffUtil()) {

    /** */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder =
        EventViewHolder.from(parent)

    /** */
    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val event = getItem(position)
        holder.bind(event = event, onRowClick = onRowClick)
    }

    /** */
    internal class EventDiffUtil : DiffUtil.ItemCallback<EventItem>() {
        /** */
        override fun areItemsTheSame(
            oldItem: EventItem,
            newItem: EventItem
        ): Boolean = oldItem == newItem

        /** */
        override fun areContentsTheSame(
            oldItem: EventItem,
            newItem: EventItem
        ): Boolean = oldItem == newItem

    }
}
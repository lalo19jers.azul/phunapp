/*
 * build.gradle
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:06
 * Copyright (c) 2021 . All rights reserved.
 */

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
}

android {

    compileSdkVersion(Sdk.COMPILE_SDK_VERSION)
    buildToolsVersion(Build.BUILD_TOOL_VERSION)

    defaultConfig {
        minSdkVersion(Sdk.MIN_SDK_VERSION)
        targetSdkVersion(Sdk.TARGET_SDK_VERSION)
        versionCode = AppCoordinates.APP_VERSION_CODE
        versionName = AppCoordinates.APP_VERSION_NAME

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }

    }

}

dependencies {
    /** KOTLIN JETBRAINS */
    implementation(Dependencies.JET_BRAINS_KOTLIN)
    /** ANDROID X */
    implementation(Dependencies.CORE_KTX)
    implementation(Dependencies.APP_COMPAT)
    /** RETROFIT */
    implementation(Dependencies.RETROFIT)
    /** MOSHI */
    implementation(Dependencies.MOSHI)
    implementation(Dependencies.MOSHI_CONVERTER)
    kapt(Dependencies.MOSHI_CODEGEN)
    /** KOIN */
    implementation(Dependencies.KOIN_ANDROID)
    implementation(Dependencies.KOIN_VIEWMODEL)
    /** MODULES */
    implementation(project(Module.DOMAIN))
    /** TESTING */
    testImplementation(TestingLib.JUNIT)
    androidTestImplementation(AndroidTestingLib.ANDROIDX_TEST_EXT_JUNIT)
    androidTestImplementation(AndroidTestingLib.ESPRESSO_CORE)
}
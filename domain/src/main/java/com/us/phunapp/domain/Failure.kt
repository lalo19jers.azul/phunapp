/*
 * Failure.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:16
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.domain

/**
 * Base Class for handling errors/failures/exceptions.
 * Every feature specific failure should extend [FeatureFailure] class.
 */
sealed class Failure {

    /**
     * Extend this class for feature specific failures.
     */
    abstract class FeatureFailure: Failure()

}
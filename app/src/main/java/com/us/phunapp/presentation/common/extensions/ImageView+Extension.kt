/*
 * ImageView+Extension.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 01:24
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation.common.extensions

import android.widget.ImageView
import coil.api.load
import coil.size.Scale
import com.us.phunapp.R

/** */
fun ImageView.loadImage(url: String?) {
    url?.let {
        if (it.isValidUrl())
            this.load(url) {
                error(R.drawable.placeholder_nomoon)
                scale(Scale.FIT)
            }
        else load(R.drawable.logo)
    }

}

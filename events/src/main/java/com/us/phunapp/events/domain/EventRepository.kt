/*
 * EventRepository.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:47
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.domain

import com.us.phunapp.datasource.local.db.entity.Event
import com.us.phunapp.domain.Either
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsFailure
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsResponse

interface EventRepository {

    /** */
    suspend fun getEvents(): Either<GetEventsFailure, GetEventsResponse>

    /** */
    suspend fun getLocalEvents(): Either<GetEventsFailure, List<Event>>
}
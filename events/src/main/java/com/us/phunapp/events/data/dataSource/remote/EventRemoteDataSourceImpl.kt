/*
 * EventRemoteDataSourceImpl.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:43
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.data.dataSource.remote

import com.us.phunapp.datasource.local.db.AppDatabase
import com.us.phunapp.datasource.remote.model.retrofitApiCall
import com.us.phunapp.domain.Either
import com.us.phunapp.events.data.dataSource.EventRemoteDataSource
import com.us.phunapp.events.data.dataSource.remote.failute.toEventFailure
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsFailure
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsResponse
import retrofit2.HttpException


internal class EventRemoteDataSourceImpl(
    private val db: AppDatabase,
    private val apiService: EventApiService
) : EventRemoteDataSource {

    /** */
    override suspend fun getEvents(): Either<GetEventsFailure, GetEventsResponse> =
        try {
            retrofitApiCall {
                apiService.getEvents()
            }.let { eventListResponse ->
                for (event in eventListResponse) {
                    db.eventDao.insert(
                        event = event.toEventDB()
                    )
                }
                Either.Right(GetEventsResponse(events = eventListResponse.map { it.toEventItem() }))
            }
        } catch (e: Exception) {
            val failure = when (e) {
                is HttpException -> e.toEventFailure()
                else -> GetEventsFailure.UnknownFailure(e.message)
            }
            Either.Left(failure)
        }
}
/*
 * EventViewHolder.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 01:18
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.us.phunapp.databinding.ItemEventBinding
import com.us.phunapp.events.domain.entity.EventItem
import com.us.phunapp.presentation.common.extensions.loadImage

class EventViewHolder(
    private val binding: ItemEventBinding
): RecyclerView.ViewHolder(binding.root) {

    /** */
    fun bind(event: EventItem, onRowClick: (EventItem) -> Unit) {
        binding.run {
            ivEvent.loadImage(url = event.image)
            aptvDate.text = event.date
            aptvTitle.text = event.title
            aptvLocationLineOne.text = event.locationline1
            aptvLocationLineTwo.text = event.locationline2
            root.setOnClickListener { onRowClick(event) }
        }
    }

    /**
     * Inflates item
     */
    companion object {
        fun from(parent: ViewGroup): EventViewHolder {
            val layoutInflater = ItemEventBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            return EventViewHolder(layoutInflater)
        }
    }
}
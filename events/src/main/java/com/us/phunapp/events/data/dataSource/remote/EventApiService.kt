/*
 * EventApiService.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:42
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.data.dataSource.remote

import com.us.phunapp.events.data.dataSource.remote.model.dto.EventItemDto
import retrofit2.Response
import retrofit2.http.GET

internal interface EventApiService {

    /** */
    @GET(URL.GET_EVENTS)
    suspend fun getEvents(): Response<List<EventItemDto>>

    private object URL {
        const val GET_EVENTS = "feed.json"
    }
}
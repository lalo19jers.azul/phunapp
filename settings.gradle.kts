/*
 * settings.gradle
 * PhunApp
 *
 * Created by lalo on 12/5/21 22:36
 * Copyright (c) 2021 . All rights reserved.
 */

include(":domain")
include(":network")
include(":dataSource")
include(":events")

include(":app")
rootProject.name = ("PhunApp")

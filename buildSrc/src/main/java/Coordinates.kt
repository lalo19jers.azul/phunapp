/*
 * Coordinate.kt
 * PhunApp
 *
 * Created by lalo on 12/5/21 22:38
 * Copyright (c) 2021 . All rights reserved.
 */

object AppCoordinates {
    const val APP_ID = "com.us.phunapp"

    const val APP_VERSION_NAME = "1.0.0"
    const val APP_VERSION_CODE = 1
}

object LibraryAndroidCoordinates {
    const val LIBRARY_VERSION = "1.0"
    const val LIBRARY_VERSION_CODE = 1
}

object LibraryKotlinCoordinates {
    const val LIBRARY_VERSION = "1.0"
}
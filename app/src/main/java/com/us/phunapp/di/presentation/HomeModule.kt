/*
 * HomeModule.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 00:06
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.di.presentation

import com.us.phunapp.presentation.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val homeModule = module {
    viewModel { HomeViewModel(getEvents = get()) }
}
/*
 * InternetConnectionApiService.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:22
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.network.internetConnection

import retrofit2.Response
import retrofit2.http.GET

interface InternetConnectionApiService {

    /**
     *
     *
     */
    @GET(URL.GENERATE_204)
    suspend fun generate204() : Response<Unit>

    /**
     *
     *
     */
    private object URL {
        /* */
        const val GENERATE_204: String = "generate_204"

    }

}
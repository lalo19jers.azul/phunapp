/*
 * GetEventsFailure.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:45
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.domain.useCase.getEvents

import com.us.phunapp.datasource.failureManage.HttpFailure
import com.us.phunapp.domain.Failure

sealed class GetEventsFailure : Failure.FeatureFailure() {
    /* */
    object NetworkConnectionFailure : GetEventsFailure()

    /* */
    object NoEventsFound : GetEventsFailure()

    /* */
    object NullPointerException: GetEventsFailure()

    /* */
    data class JsonDataDeserializationFailure(val message: String?) : GetEventsFailure()

    /* */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetEventsFailure(), HttpFailure

    /* */
    data class UnknownFailure(val message: String?) : GetEventsFailure()

    /** */
    companion object {
        internal fun fromFeatureFailure(failure: Failure) = when (failure) {
            is GetEventsFailure -> failure
            else -> UnknownFailure(failure.javaClass.simpleName)
        }
    }
}
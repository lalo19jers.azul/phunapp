/*
 * HttpClientModule.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:38
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.datasource

import com.us.phunapp.datasource.remote.httpClient.HeaderInterceptor
import com.us.phunapp.datasource.remote.httpClient.HeaderInterceptorImpl
import com.us.phunapp.datasource.remote.httpClient.RetrofitBuilder
import org.koin.dsl.module

/* */
val httpClientModule = module {

    /** RETROFIT BUILDER */
    single {
        RetrofitBuilder(
            baseUrl = BuildConfig.API_BASE_URL,
            headerInterceptor = get()
        ).build()
    }

    /** HEADER INTERCEPTOR */
    single<HeaderInterceptor> {
        HeaderInterceptorImpl()
    }

}
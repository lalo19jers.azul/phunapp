/*
 * build.gradle.kts
 * PhunApp
 *
 * Created by lalo on 12/5/21 22:36
 * Copyright (c) 2021 . All rights reserved.
 */

plugins{
    `kotlin-dsl`
}

repositories {
    jcenter()
}
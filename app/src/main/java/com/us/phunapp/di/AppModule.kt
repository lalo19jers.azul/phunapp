/*
 * AppModule.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:49
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.di

import com.us.phunapp.PhunApp
import com.us.phunapp.datasource.httpClientModule
import com.us.phunapp.datasource.roomModule
import com.us.phunapp.di.presentation.homeModule
import com.us.phunapp.events.eventsModule
import com.us.phunapp.network.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.module.Module

/**
 * This class is our dependency injection. Built with koin.
 * Our App architecture is Clean Architecture + MVVM.
 * And we change the Viewmodel factory with this dependency injection.
 * If you need more parameters will be welcome in this module.
 */
fun PhunApp.initKoin() {
    startKoin {
        val modules = getPresentationModules() + getSharedModules() + getFeatureModules()
        androidLogger(Level.ERROR)
        androidContext(applicationContext)
        modules(modules)
    }
}

/**
 *
 * @return [List]
 */
private fun getSharedModules(): List<Module> = listOf(
    /** **/
    networkModule,
    httpClientModule,
    roomModule

)

/**
 *
 * @return [List]
 */
private fun getFeatureModules(): List<Module> = listOf(
    eventsModule
)

/**
 * Presentation module is the layer that interacts with UI.
 * Presentation layer contains ViewModel, Fragments and Activities.
 * @return [List]
 */
private fun getPresentationModules(): List<Module> = listOf(
    /**  **/
    homeModule

)
/*
 * NetworkModule.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:21
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.network

import com.us.phunapp.network.internetConnection.InternetConnectionApiService
import com.us.phunapp.network.internetConnection.InternetConnectionRepository
import com.us.phunapp.network.internetConnection.InternetConnectionRepositoryImpl
import com.us.phunapp.network.internetConnection.InternetConnectionRetrofitBuilder
import org.koin.dsl.module

/* */
val networkModule = module {

    /* Internet connection repository instance */
    single<InternetConnectionRepository>(createdAtStart = true) {
        InternetConnectionRepositoryImpl()
    }
    /* */
    single<InternetConnectionApiService> {
        InternetConnectionRetrofitBuilder()
            .build()
            .create(InternetConnectionApiService::class.java)
    }

}
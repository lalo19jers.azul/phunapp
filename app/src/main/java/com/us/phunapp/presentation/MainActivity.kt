/*
 * MainActivity.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:50
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.us.phunapp.R
import com.us.phunapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    /* */
    private val binding: ActivityMainBinding
            by lazy { ActivityMainBinding.inflate(layoutInflater) }

    /* */
    private lateinit var navController: NavController

    /** */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        navController = findNavController(R.id.activity_main_navHost)
        setupToolbar()
    }

    /** */
    private fun setupToolbar() {
        binding.toolbar.setupWithNavController(navController)
    }

}
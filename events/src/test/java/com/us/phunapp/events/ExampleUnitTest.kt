/*
 * ExampleUnitTest.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:27
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}
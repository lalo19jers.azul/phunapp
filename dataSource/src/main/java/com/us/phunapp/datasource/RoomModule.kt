/*
 * RoomModule.kt
 * PhunApp
 *
 * Created by lalo on 15/5/21 00:39
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.datasource

import com.us.phunapp.datasource.local.db.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/* */
val roomModule = module {

    /* */
    single { AppDatabase.getInstance(androidContext()) }

}
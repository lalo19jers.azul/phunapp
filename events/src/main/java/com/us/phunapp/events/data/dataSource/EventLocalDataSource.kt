/*
 * EventLocalDataSource.kt
 * PhunApp
 *
 * Created by lalo on 15/5/21 01:07
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.data.dataSource

import com.us.phunapp.datasource.local.db.entity.Event
import com.us.phunapp.domain.Either
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsFailure

interface EventLocalDataSource {

    /** */
    suspend fun getEvents(): Either<GetEventsFailure, List<Event>>
}
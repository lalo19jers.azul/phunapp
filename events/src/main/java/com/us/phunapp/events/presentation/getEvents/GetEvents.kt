/*
 * GetEvents.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:51
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.presentation.getEvents

import androidx.lifecycle.LiveData
import com.us.phunapp.domain.Either
import com.us.phunapp.domain.Status
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsFailure
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsResponse

/** */
typealias GetEventsStatus =
        Status<GetEventsFailure, GetEventsResponse>

interface GetEvents {

    /* */
    var getEventsResponse: GetEventsResponse

    /* */
    var getEventsFailure: GetEventsFailure

    /** */
    fun getEventsAsLiveData(): LiveData<GetEventsStatus>

    /** */
    suspend fun getEventsAsEither(): Either<GetEventsFailure, GetEventsResponse>
}
/*
 * HttpException+Extension.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:43
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.data.dataSource.remote.failute

import com.us.phunapp.datasource.remote.model.HttpErrorCode
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsFailure
import retrofit2.HttpException

/** */
internal fun HttpException.toEventFailure(): GetEventsFailure =
    when(HttpErrorCode.from(code())) {
        HttpErrorCode.HTTP_NOT_FOUND -> GetEventsFailure.NoEventsFound
        else -> GetEventsFailure.ServerFailure(code(),message())
    }
/*
 * PhunApp.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:50
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp

import android.app.Application
import com.us.phunapp.di.initKoin
import timber.log.Timber

class PhunApp: Application() {

    /** */
    override fun onCreate() {
        super.onCreate()
        initKoin()
        Timber.plant(Timber.DebugTree())
    }

}
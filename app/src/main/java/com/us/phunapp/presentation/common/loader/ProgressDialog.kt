/*
 * ProgressDialog.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 00:03
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation.common.loader

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.us.phunapp.R
import com.us.phunapp.databinding.ProgressDialogBinding

object ProgressDialog {

    /* */
    @SuppressLint("StaticFieldLeak")
    private lateinit var binding: ProgressDialogBinding
    /* */
    private var dialog: Dialog? = null

    /** */
    fun show(context: Context, message: String? = null) {
        binding = ProgressDialogBinding.inflate(LayoutInflater.from(context))
        dialog = Dialog(context, R.style.AppTheme_FullDialog)
        binding.dialogProgressCircleMessage.visibility = if (message.isNullOrBlank()) View.GONE else View.VISIBLE
        binding.dialogProgressCircleMessage.text = message
        dialog?.setContentView(binding.root)
        dialog?.setCancelable(false)
        dialog?.show()
    }

    /** */
    fun dismiss() {
        if (dialog == null) return
        dialog?.dismiss()
    }
}
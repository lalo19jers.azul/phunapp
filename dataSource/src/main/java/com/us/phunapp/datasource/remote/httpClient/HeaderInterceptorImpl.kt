/*
 * HeaderInterceptorImpl.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:37
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.datasource.remote.httpClient

class HeaderInterceptorImpl: HeaderInterceptor {

    /**
     * @return [String]
     */
    override fun getAuthorizationType(): String = "JWT"

    /**
     * @return [String]
     */
    override fun getAuthorizationValue(): String? {
        return null
    }

}
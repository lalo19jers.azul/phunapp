/*
 * InternetConnectionRepository.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:23
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.network.internetConnection

import androidx.lifecycle.LiveData

interface InternetConnectionRepository  {
    /* */
    val isOnline: Boolean
    /* */
    val isOnLineLiveData: LiveData<Boolean>

    /** */
    suspend fun fetch()
}
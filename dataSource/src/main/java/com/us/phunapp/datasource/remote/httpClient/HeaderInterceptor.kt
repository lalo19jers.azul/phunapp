/*
 * HeaderInterceptor.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:36
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.datasource.remote.httpClient

interface HeaderInterceptor {

    /**
     * This authorization type goes in the header (if interceptor exists).
     * e.g. JWT, Bearer, etc.
     *
     * @return Type in [String] format.
     */
    fun getAuthorizationType(): String

    /**
     * This value could be a token.
     * e.g. JWT value
     *
     * @return Value in [String] format.
     */
    fun getAuthorizationValue(): String?

}
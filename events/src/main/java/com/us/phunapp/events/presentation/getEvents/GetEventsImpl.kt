/*
 * GetEventsImpl.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:51
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.presentation.getEvents

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import com.us.phunapp.domain.Either
import com.us.phunapp.domain.Status
import com.us.phunapp.domain.onFailure
import com.us.phunapp.domain.onRight
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsFailure
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsParams
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsResponse
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow

class GetEventsImpl(
    private val getEventsUseCase: GetEventsUseCase
) : GetEvents {

    /* */
    override lateinit var getEventsResponse: GetEventsResponse

    /* */
    override lateinit var getEventsFailure: GetEventsFailure

    /** */
    override fun getEventsAsLiveData(): LiveData<GetEventsStatus> = flow<GetEventsStatus> {
        emit(Status.Loading())
        getEventsAsEither()
            .onFailure { emit(Status.Error(it)) }
            .onRight {  emit(Status.Done(it))  }
    }.asLiveData(Dispatchers.IO)

    /** */
    override suspend fun getEventsAsEither():
            Either<GetEventsFailure, GetEventsResponse> =
        getEventsUseCase.run(GetEventsParams)
            .onFailure { getEventsFailure = it }
            .onRight { getEventsResponse = it }

}
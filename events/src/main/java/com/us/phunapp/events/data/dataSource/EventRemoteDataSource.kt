/*
 * EventRemoteDataSource.kt
 * PhunApp
 *
 * Created by lalo on 15/5/21 01:57
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.data.dataSource

import com.us.phunapp.domain.Either
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsFailure
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsResponse

internal interface EventRemoteDataSource {

    /** */
    suspend fun getEvents(
    ): Either<GetEventsFailure, GetEventsResponse>
}
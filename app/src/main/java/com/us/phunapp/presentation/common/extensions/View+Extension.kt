/*
 * View+Extension.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 00:00
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation.common.extensions

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.us.phunapp.R

/** */
fun Fragment.showProgressBar(messageRes: Int = R.string.default_loading_message) =
    requireContext().showProgressBar(messageRes)

/** */
fun Fragment.hideProgressBar() = requireContext().hideProgressBar()

fun View.presentShortSnackBar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}
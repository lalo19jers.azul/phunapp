/*
 * HttpFailure.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:31
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.datasource.failureManage

/** */
interface HttpFailure {

    /** Server failure code */
    val code: Int

    /** Server failure message */
    val message: String

}
/*
 * DetailEventFragment.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 02:01
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation.eventDetail

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.us.phunapp.R
import com.us.phunapp.databinding.DetailEventFragmentBinding
import com.us.phunapp.events.domain.entity.EventItem
import com.us.phunapp.presentation.common.extensions.loadImage
import com.us.phunapp.presentation.common.extensions.presentShortSnackBar


class DetailEventFragment : Fragment() {

    //region INIT COMPONENTS

    /* */
    private val binding: DetailEventFragmentBinding
            by lazy { DetailEventFragmentBinding.inflate(layoutInflater) }

    /* */
    private val args: DetailEventFragmentArgs by navArgs()

    //endregion

    //region LIFECYCLE

    /** */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    /** */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                args.event.phone?.let { phone ->
                    didTapCall(
                        callAtPhone = phone.replace(" ", "").trim()
                    )
                }
            } else {
                binding.root.presentShortSnackBar(
                    message = getString(R.string.permissions_denied)
                )            }
        }
    }
    //endregion

    //region SETUP VIEW

    /** */
    private fun setUpView() {
        binding.apply {
            eventDetailDescription.text = args.event.description
            eventDetailLocationLineOne.text = args.event.locationline2
            eventDetailTimestamp.text = args.event.timestamp
            eventDetailTitle.text = args.event.title
            eventDetailIv.loadImage(url = args.event.image)
            eventDetailIvShare.setOnClickListener { shareWith(event = args.event) }
            eventDetailIvPhone.setOnClickListener {
                args.event.phone?.let { phone ->
                    didTapCall(
                        callAtPhone = phone.replace(" ", "").trim()
                    )
                }
            }
        }
    }

    /** */
    private fun shareWith(event: EventItem) {
        val shareHeader = getString(R.string.share_info_title_header, event.title, event.date)
        val imageUri: Uri = Uri.parse(event.image)
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareHeader)
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
        shareIntent.type = getString(R.string.share_image_type)
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share)))
    }

    /** */
    private fun didTapCall(callAtPhone: String) {
        try {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.CALL_PHONE),
                    101
                )
                return
            }
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse(getString(R.string.share_phone_type, callAtPhone))
            startActivity(callIntent)

        } catch (ex: Exception) {
            binding.root.presentShortSnackBar(
                message = ex.message ?: getString(R.string.action_denied)
            )
        }
    }
    //endregion
}
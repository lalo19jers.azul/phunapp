/*
 * Event.kt
 * PhunApp
 *
 * Created by lalo on 15/5/21 00:15
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.datasource.local.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

typealias dbEvent = Event

@Entity(tableName = "event")
data class Event(
    @PrimaryKey @ColumnInfo(name = "event_id") val id: Int,
    val date: String,
    val description: String,
    val image: String?,
    @ColumnInfo(name = "location_line_one") val locationline1: String,
    @ColumnInfo(name = "location_line_two") val locationline2: String,
    val phone: String?,
    val timestamp: String,
    val title: String
)

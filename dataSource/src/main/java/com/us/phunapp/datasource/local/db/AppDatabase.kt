/*
 * AppDatabase.kt
 * PhunApp
 *
 * Created by lalo on 15/5/21 00:40
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.datasource.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.us.phunapp.datasource.local.db.dao.EventDao
import com.us.phunapp.datasource.local.db.entity.Event

@Database(
    entities = [
        Event::class
               ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    /* */
    abstract val eventDao: EventDao

    /** Creates database instance */
    companion object {
        // Stores a strong reference to database object and this variable helps to
        // know if the reference exists or should be created.
        private var INSTANCE: AppDatabase? = null

        /**
         * Gets an ´AppDatabase´ instance.
         * First off, room builder is called and the context, reference class and database name
         * is provided, and builds the new instance.
         *
         * @param context Application context.
         */
        fun getInstance(context: Context): AppDatabase = INSTANCE ?: synchronized(this) {
            var instance = INSTANCE
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "pw_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
            }
            instance
        }
    }
}
/*
 * EventItem.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:38
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EventItem(
    val date: String,
    val description: String,
    val id: Int,
    val image: String?,
    val locationline1: String,
    val locationline2: String,
    val phone: String?,
    val timestamp: String,
    val title: String
): Parcelable
/*
 * EventItemDto.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 21:59
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.data.dataSource.remote.model.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.us.phunapp.datasource.local.db.entity.dbEvent
import com.us.phunapp.events.domain.entity.EventItem

@JsonClass(generateAdapter = true)
data class EventItemDto(
    @Json(name = "date") val date: String,
    @Json(name = "description") val description: String,
    @Json(name = "id") val id: Int,
    @Json(name = "image") val image: String?,
    @Json(name = "locationline1") val locationline1: String,
    @Json(name = "locationline2") val locationline2: String,
    @Json(name = "phone") val phone: String?,
    @Json(name = "timestamp") val timestamp: String,
    @Json(name = "title") val title: String
) {

    /** */
    fun toEventItem(): EventItem =
        EventItem(
            date = date,
            description = description,
            id = id,
            image = image,
            locationline1 = locationline1,
            locationline2 = locationline2,
            phone = phone,
            timestamp = timestamp,
            title = title
        )

    /** */
    fun toEventDB(): dbEvent =
        dbEvent(
            id = id,
            date = date,
            description = description,
            image = image,
            locationline1 = locationline1,
            locationline2 = locationline2,
            phone = phone,
            timestamp = timestamp,
            title = title
        )
}
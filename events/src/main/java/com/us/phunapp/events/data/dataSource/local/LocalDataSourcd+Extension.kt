/*
 * LocalDataSourcd+Extension.kt
 * PhunApp
 *
 * Created by lalo on 15/5/21 01:03
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.data.dataSource.local

import com.us.phunapp.datasource.local.db.entity.Event

/** */
internal suspend fun EventLocalDataSourceImpl.getAllEvents(): List<Event> =
    db.eventDao.geEvents()
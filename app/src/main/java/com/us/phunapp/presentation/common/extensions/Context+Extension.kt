/*
 * Context+Extension.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 00:00
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation.common.extensions

import android.content.Context
import android.content.Intent
import com.us.phunapp.R
import com.us.phunapp.presentation.common.loader.ProgressDialog

/** */
fun Context.showProgressBar(messageRes: Int = R.string.default_loading_message) =
    ProgressDialog.show(this, message = getString(messageRes))

/** */
fun Context.hideProgressBar() = ProgressDialog.dismiss()

/** */
fun <T> Context.navigateTo(javaClass: Class<T>, clearTop: Boolean = false) {
    Intent(this, javaClass).apply {
        if (clearTop)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(this)
    }
}
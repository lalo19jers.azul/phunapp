/*
 * InternetConnectionRepositoryImpl.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:23
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.network.internetConnection

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject


internal class InternetConnectionRepositoryImpl
    :InternetConnectionRepository, KoinComponent{

    /* */
    private val internetConnectionApiService: InternetConnectionApiService by inject()

    /* */
    private var _isOnline: Boolean = false
    override val isOnline: Boolean get() = _isOnline

    /* */
    private var _isOnlineLiveData = MutableLiveData<Boolean>()
    override val isOnLineLiveData: LiveData<Boolean>
        get() = _isOnlineLiveData

    /** */
    override suspend fun fetch() {
        _isOnline = try {
            internetConnectionApiService.generate204()
            true
        } catch (exception: Exception) {false}
        _isOnlineLiveData.postValue(_isOnline)
    }

}
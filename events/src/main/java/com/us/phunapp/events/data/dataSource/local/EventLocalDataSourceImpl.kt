/*
 * EventLocalDataSourceImpl.kt
 * PhunApp
 *
 * Created by lalo on 15/5/21 01:03
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.data.dataSource.local

import com.us.phunapp.datasource.local.db.AppDatabase
import com.us.phunapp.datasource.local.db.entity.Event
import com.us.phunapp.domain.Either
import com.us.phunapp.events.data.dataSource.EventLocalDataSource
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsFailure

internal class EventLocalDataSourceImpl(
    val db: AppDatabase
): EventLocalDataSource {

    /** */
    override suspend fun getEvents(): Either<GetEventsFailure, List<Event>> =
        try {
            Either.Right(getAllEvents())
        } catch (e: Exception) {
            val failure = when(e) {
                is NullPointerException -> GetEventsFailure.NullPointerException
                else -> GetEventsFailure.UnknownFailure(e.message)
            }
            Either.Left(failure)
        }

}
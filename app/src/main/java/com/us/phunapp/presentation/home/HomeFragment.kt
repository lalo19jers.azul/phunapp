/*
 * HomeFragment.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 01:18
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.us.phunapp.databinding.HomeFragmentBinding
import com.us.phunapp.domain.Status
import com.us.phunapp.events.domain.entity.EventItem
import com.us.phunapp.events.presentation.getEvents.GetEventsStatus
import com.us.phunapp.presentation.common.extensions.hideProgressBar
import com.us.phunapp.presentation.common.extensions.presentShortSnackBar
import com.us.phunapp.presentation.common.extensions.showProgressBar
import com.us.phunapp.presentation.home.adapter.EventAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    //region INIT COMPONENTS
    /* */
    private val binding: HomeFragmentBinding
            by lazy { HomeFragmentBinding.inflate(layoutInflater) }

    /* */
    private val viewModel: HomeViewModel by viewModel()

    /* */
    private val eventAdapter: EventAdapter
            by lazy { EventAdapter(onRowClick = onRowClick) }

    //endregion

    //region LIFECYCLE
    /** */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    /** */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        execute()
    }

    //endregion

    //region GET EVENTS
    /** */
    private fun execute() {
        viewModel.getEventsAsLiveData().observe(
            viewLifecycleOwner, handleEventsStatusObserver()
        )
    }

    /** */
    private fun handleEventsStatusObserver() = Observer<GetEventsStatus> {
        requireContext().hideProgressBar()
        when (it) {
            is Status.Loading -> requireContext().showProgressBar()
            is Status.Error -> binding.root.presentShortSnackBar(it.failure.toString())
            is Status.Done -> setUpView(events = it.value.events)
        }
    }

    //endregion

    //region SETUP VIEW

    /** */
    private fun setUpView(events: List<EventItem>) {
        binding.apply {
            eventAdapter.submitList(events)
            homeFragmentRvEvents.adapter = eventAdapter
        }
    }

    /** */
    private val onRowClick: (EventItem) -> Unit = {
        val direction = HomeFragmentDirections.actionHomeFragmentToDetailEventFragment(
            event = it
        )
        findNavController().navigate(direction)
    }

    //endregion


}
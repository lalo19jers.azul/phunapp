/*
 * EventRepositoryImpl.kt
 * PhunApp
 *
 * Created by lalo on 15/5/21 01:07
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events.data.dataSource

import com.us.phunapp.datasource.local.db.entity.Event
import com.us.phunapp.domain.Either
import com.us.phunapp.events.domain.EventRepository
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsFailure
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsResponse
import com.us.phunapp.network.internetConnection.InternetConnectionRepository

internal class EventRepositoryImpl(
    private val eventRemoteDataSource: EventRemoteDataSource,
    private val eventLocalDataSource: EventLocalDataSource,
    internetConnectionRepository: InternetConnectionRepository
) : EventRepository, InternetConnectionRepository by internetConnectionRepository {

    /** */
    override suspend fun getEvents():
            Either<GetEventsFailure, GetEventsResponse> =
        eventRemoteDataSource.getEvents()
    /*
    How it should be works:
    if (isOnline)
            eventRemoteDataSource.getEvents()
    else eventLocalDataSource.getEvents()
     */

    override suspend fun getLocalEvents(): Either<GetEventsFailure, List<Event>> =
        eventLocalDataSource.getEvents()

    /**
     * NOTE: for some reason variable [isOnline] always returns false.
     * I don't have more time to check and fix this issue but code commented above
     * is an example that how [isOnline] help us to verify is we connect to remoteDataSource
     * or localDataSource.
     * And this way I decided not change [GetEventsResponse] from List<EventItem>(model entity) to
     * List<Event> (db entity)
     */

}
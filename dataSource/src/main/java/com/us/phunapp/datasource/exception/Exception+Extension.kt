/*
 * Exception+Extension.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:30
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.datasource.exception

/** */
fun Exception.message(): String = message ?: tag

/** */
val Exception.tag: String
    get() = "${javaClass.simpleName} - $message"
/*
 * String+Extension.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 01:49
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation.common.extensions

import android.util.Patterns

/** */
fun String.isValidUrl() =
    Patterns.WEB_URL.matcher(this).matches()

/*
 * EventDao.kt
 * PhunApp
 *
 * Created by lalo on 15/5/21 00:25
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.datasource.local.db.dao

import androidx.room.*
import com.us.phunapp.datasource.local.db.entity.Event

@Dao
interface EventDao {

    /** */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(event: Event)

    /** */
    @Transaction
    @Query("SELECT * FROM event")
    suspend fun geEvents(): List<Event>
}
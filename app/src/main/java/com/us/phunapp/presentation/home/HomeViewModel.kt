/*
 * HomeViewModel.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 01:55
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.presentation.home

import androidx.lifecycle.ViewModel
import com.us.phunapp.events.presentation.getEvents.GetEvents

class HomeViewModel(
    getEvents: GetEvents
) : ViewModel() , GetEvents by getEvents
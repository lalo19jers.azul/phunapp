/*
 * build.gradle
 * PhunApp
 *
 * Created by lalo on 12/5/21 22:36
 * Copyright (c) 2021 . All rights reserved.
 */

// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {

    repositories {
        google()
        jcenter()
    }

    dependencies {
        classpath (AndroidTools.CLASS_PATH_ANDROID_TOOLS_GRADLE)
        classpath(Kotlin.GRADLE_PLUGIN)
        classpath (Dependencies.NAVIGATION_SAFE_ARGS)
    }

}

allprojects {

    repositories {
        google()
        jcenter()
        maven { setUrl("https://jitpack.io") }
    }

}

tasks.register("clean", Delete::class.java) {
    delete(rootProject.buildDir)
}
/*
 * EventsModule.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 22:25
 * Copyright (c) 2021 . All rights reserved.
 */

package com.us.phunapp.events

import com.us.phunapp.events.data.dataSource.EventLocalDataSource
import com.us.phunapp.events.data.dataSource.EventRemoteDataSource
import com.us.phunapp.events.data.dataSource.EventRepositoryImpl
import com.us.phunapp.events.data.dataSource.local.EventLocalDataSourceImpl
import com.us.phunapp.events.data.dataSource.remote.EventApiService
import com.us.phunapp.events.data.dataSource.remote.EventRemoteDataSourceImpl
import com.us.phunapp.events.domain.EventRepository
import com.us.phunapp.events.domain.useCase.getEvents.GetEventsUseCase
import com.us.phunapp.events.presentation.getEvents.GetEvents
import com.us.phunapp.events.presentation.getEvents.GetEventsImpl
import org.koin.dsl.module
import retrofit2.Retrofit

val eventsModule = module {

    /** PRESENTATION */
    single<GetEvents> { GetEventsImpl(getEventsUseCase = get()) }

    /** USE CASE */
    factory { GetEventsUseCase(repository = get()) }

    /** REPOSITORY */
    single<EventRepository> {
        EventRepositoryImpl(
            eventRemoteDataSource = get(),
            eventLocalDataSource = get(),
            internetConnectionRepository = get()
        )
    }

    /** DATA SOURCE REMOTE */
    single<EventRemoteDataSource> {
        EventRemoteDataSourceImpl(
            apiService = get(),
            db = get()
        )
    }

    /** LOCAL DATA SOURCE */
    single<EventLocalDataSource> {
        EventLocalDataSourceImpl(
            db = get()
        )
    }

    /** API SOURCE */
    single { get<Retrofit>().create(EventApiService::class.java) }

}